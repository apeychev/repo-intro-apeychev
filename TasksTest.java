import org.junit.Assert;
import org.junit.Test;

public class Tasks2Test {

	Tasks taskInstance = new Tasks();
	
	@Test
	public void testIsOddWhenNumberIsOddReturnTrue() {
		Assert.assertTrue(taskInstance.isOdd(5));
	}
	
	@Test
	public void testIsOddWhenNumberIsZeroReturnFalse(){
		Assert.assertFalse(taskInstance.isOdd(0));
	}
	
	@Test
	public void testIsOddWhenNumberIsNegativeEvenReturnFalse() {
		Assert.assertFalse(taskInstance.isOdd(-80));
	}
	
	@Test
	public void testIsPrimeWhenNumberIsPrimeReturnTrue() {
		Assert.assertTrue(taskInstance.isPrime(53));
	}

	@Test
	public void testIsPrimeWhenNumberIs0ReturnFalse() {
		Assert.assertFalse(taskInstance.isPrime(0));
	}

	@Test
	public void testIsPrimeWhenNumberIs1ReturnFalse() {
		Assert.assertFalse(taskInstance.isPrime(1));
	}

	@Test
	public void testIsPrimeWhenNumberIsNegativeReturnFalse() {
		Assert.assertFalse(taskInstance.isPrime(-5));
	}
	
	@Test
	public void testMinGivenArrOfNumsWhenArrIsWitPositiveNumsReturnMinNum() {
		Assert.assertEquals(3, taskInstance.min(102, 66, 74, 3, 11, 104, 5, 89, 218, 360, 4, 370, 3));
	}

	@Test
	public void testMinGivenArrOfNumsWhenArrIsWithNegativeNumsReturnMinNum() {
		Assert.assertEquals(-213124312, taskInstance.min(-12350, 12350, -12350, 12350, 12350, -3432432, -213124312));
	}
	
	@Test
	public void testKthMinGivenNumAndArrOfNums_WhenNumIsK_And_ArrIsWithPositiveNums_ReturnKthMinNumber() {
		Assert.assertEquals(5,
				taskInstance.kthMin(3, new int[] { 102, 66, 74, 3, 11, 104, 5, 89, 218, 360, 4, 370, 3 }));
	}

	@Test
	public void testKthMinGivenNumberAndArrOfNums_WhenNumIsK_And_ArrIsWithNegativeNums_ReturnKthMinNumber() {
		Assert.assertEquals(-66,
				taskInstance.kthMin(8, new int[] { -102, -66, -74, 3, 11, -104, 5, -89, -218, -360, 4, -370, 3 }));
	}
	
	@Test
	public void testGetOddOccurrenceGivenArrOfNumsReturnTheNumThatOccuredOddTimes() {
		Assert.assertEquals(32, taskInstance.getOddOccurrence(56, 32, 56, 32, 17, 32, 17, 62, 62));
	}

	@Test
	public void testGetOddOccurrenceGivenArrOfNumsWhenNumOccuresOnesReturnTheNumberThatOccuredOddTimes() {
		Assert.assertEquals(104, taskInstance.getOddOccurrence(104, 108, 109, 108, 109, 67, 76, 67, 76));
	}
	
	@Test
	public void testGetAverageGivenArrOfNumsReturnAverageOfNums() {
		Assert.assertEquals(69, taskInstance.getAverage(new int[] { 6, 66, 38, 94, 105, 108 }));
	}

	@Test
	public void testGetAverageDoubleWhenUsingAssertTrueReturnDouble() {
		Assert.assertTrue(Math.abs(54.714285714285714285714285714286
				- taskInstance.getAverageDouble(new int[] { 56, 89, 45, 23, 78, 25, 67 })) < 0.001);
	}

	@Test
	public void testGetAvergaeDoubleWhenUsingAssertEqualsWithDeltaReturnDouble() {
		Assert.assertNotEquals(54.716, taskInstance.getAverageDouble(new int[] { 56, 89, 45, 23, 78, 25, 67 }), 0.001);
	}
	
}