public class Tasks {

	public boolean isOdd(int number) {
		if (number % 2 == 1 || number % 2 == -1) {
			return true;
		}
		return false;
	}
	
	public boolean isPrime(int n) {
		if (n <= 1)
			return false;
		for (int i = 2; i < n; i++) {
			if (n % i == 0) {
				return false;
			}
		}
		return true;

	}
	
	public int min(int... array) {
		int min = 2147483647;

		for (int i = 0; i < array.length; i++) {
			if (array[i] < min) {
				min = array[i];
			}
		}
		return min;
	}
	
	public int kthMin(int k, int[] array) {

		for (int i = 0; i < k; i++) {
			int currentMinimum = array[i];
			int currentMinimumIndex = i;
			for (int j = i + 1; j < array.length; j++) {
				if (array[j] == currentMinimum) {
					array[j] = 2147483647;
				}
				if (array[j] < currentMinimum) {
					currentMinimum = array[j];
					currentMinimumIndex = j;
				}
			}
			int temp = array[i];
			array[i] = array[currentMinimumIndex];
			array[currentMinimumIndex] = temp;
		}
		return array[k - 1];
	}
	
	public int getOddOccurrence(int... array) {
		Map<Integer, Integer> map = new HashMap<>();
		for (int i = 0; i < array.length; i++) {
			if (map.containsKey(array[i])) {
				int occurance = map.get(array[i]);
				occurance++;
				map.put(array[i], occurance);
			} else {
				map.put(array[i], 1);
			}
		}

		for (Entry<Integer, Integer> number : map.entrySet()) {
			if (number.getValue() % 2 == 1)
				return number.getKey();
		}
		return 0;
	}
	
	public static int getAverage(int[] array) {

		int sum = 0;

		for (int i = 0; i < array.length; i++) {
			sum += array[i];
		}

		return sum / array.length;
	}

	public double getAverageDouble(int[] array) {

		int sum = 0;

		for (int i = 0; i < array.length; i++) {
			sum += array[i];
		}

		double average = (double) sum / (double) array.length;

		return average;

	}
	
}